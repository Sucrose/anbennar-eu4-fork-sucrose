#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 48  51  108 }

revolutionary_colors = { 13  8  13 }

historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	economic_ideas
	offensive_ideas
	expansion_ideas
	quality_ideas	
	administrative_ideas	
	influence_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = {
	"Welyam #3" = 90
	"Willam #0" = 70
	"James #3" = 60
	"Edmund #3" = 100
	"Colyn #2" = 40
	"Toman #1" = 40
	"Humbert #3" = 80
	"Godrac #12" = 3
	"Godryc #3" = 10
	"Godric #0" = 70
	"Stovac #1" = 5
	"Ricard #1" = 55
	"Alenn #3" = 55
	"Ulric #3" = 5
	"Carlan #2" = 20
	
	
	"Acromar #0" = 10
	"Adelar #0" = 10
	"Alain #0" = 10
	"Alen #0" = 10
	"Arnold #0" = 10
	"Camir #0" = 10
	"Camor #0" = 10
	"Canrec #0" = 10
	"Celgal #0" = 10
	"Ciramod #0" = 10
	"Clarimond #0" = 10
	"Clothar #0" = 10
	"Coreg #0" = 10
	"Crovan #0" = 10
	"Crovis #0" = 10
	"Corac #0" = 10
	"Corric #0" = 10
	"Dalyon #0" = 10
	"Delia #0" = 10
	"Delian #0" = 10
	"Devac #0" = 10
	"Devan #0" = 10
	"Dustyn #0" = 10
	"Elecast #0" = 10
	"Frederic #0" = 10
	"Godwin #0" = 10
	"Gracos #0" = 10
	"Henric #0" = 10
	"Humac #0" = 10
	"Humban #0" = 10
	"Humbar #0" = 10
	"Jacob #0" = 10
	"Lain #0" = 10
	"Lan #0" = 10
	"Madalac #0" = 10
	"Marcan #0" = 10
	"Ottrac #0" = 10
	"Ottran #0" = 10
	"Petran #0" = 10
	"Peyter #0" = 10
	"Rabac #0" = 10
	"Rabard #0" = 10
	"Rycan #0" = 10
	"Rican #0" = 10
	"Rogec #0" = 10
	"Roger #0" = 10
	"Stovan #0" = 10
	"Teagan #0" = 10
	"Tomac #0" = 10
	"Tomar #0" = 10
	"Tormac #0" = 10
	"Venac #0" = 10
	"Vencan #0" = 10
	"Walter #0" = 10
	"Wystan #0" = 10
	"Bellac #0" = 10
	

	"Adela #0" = -10
	"Alice #0" = -10
	"Anna #0" = -10
	"Anne #0" = -10
	"Auci #0" = -10
	"Bella #0" = -10
	"Catherine #0" = -10
	"Clarimonde #0" = -10
	"Clarya #0" = -10
	"Clothilde #0" = -10
	"Constance #0" = -10
	"Cora #0" = -10
	"Coralinde #0" = -10
	"Dalya #0" = -10
	"Edith #0" = -10
	"Eleanor #0" = -10
	"Elenor #0" = -10
	"Emma #0" = -10
	"Etta #0" = -10
	"Humba #0" = -10
	"Lisban #0" = -10
	"Lisbet #0" = -10
	"Madala #0" = -10
	"Magda #0" = -10
	"Matilda #0" = -10
	"Robyn #0" = -10
}

leader_names = {
	Gerwick "of Vanbury" Eaglecrest Fludd Lodan Feycombe Wightsgate Humbercroft Ginnfield Derwing Alainson Cottersea Leapdon Brontay Beron Cockerwall Edgemoor Fouler Lonevalley Greenley "of Alenath" 
	Norleigh "of Morban" Jonsway Mossford Oudesker Wolfden Aldtempel Gardfort Drakesford Somberwold Westfield Baldfather Alenfield "of the Greatmarch"
	
	"of Northburn" "of Elderhollow" "of Bluesfield" Redgrave Morton Winters Brooks Robinson Alenson Woods Oldfort Newfort Eaglefort Horsefort Gilmore Swyft Mottram Rackham Thaxton Barton Bateman
	Curran Doe Elk Avery "of Ashby" "of Vineroot Hollow" Smith Hammerford Croxford Shoebridge Slowbridge Guardbridge Butler Broadhurst Mason "s�l Wex" "of Vinerick" Moss Moore Reed Wicknman Potts Toadswell Wood Townsend
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
}

army_names = {
	"Royal Army" "Army of the Eagle" "Army of the Talon" "Army of $PROVINCE$" "Army of the Greatmarches" "Army of the Moors" "Army of the Alen"
}

fleet_names = {
	"Royal Navy" "Damescrown Squadron" "Dameshead Squadron" "Damesneck Squadron" "Blue Squadron" "Eagle Squadron" "Hawk Squadron" "Castle Squadron" "Shield Squadron"
}