#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 201  118  177 }

revolutionary_colors = { 201  118  177 }

historical_idea_groups = {
	economic_ideas
	offensive_ideas
	exploration_ideas
	defensive_ideas	
	administrative_ideas	
	maritime_ideas
	quality_ideas
	innovativeness_ideas
}
historical_units = {
	cannorian_1_medieval_infantry
	cannorian_1_medieval_knights
	cannorian_5_men_at_arms
	cannorian_9_pike_and_shot
	cannorian_10_gun_knights
	cannorian_12_thorn_formation
	cannorian_14_outriders
	cannorian_15_volley_infantry
	cannorian_18_reformed_outriders
	cannorian_19_thorn_formation2
	cannorian_23_line_infantry
	cannorian_26_carabiners
	cannorian_26_rosecoat_infantry
	cannorian_28_impulse_infantry
	cannorian_28_cuirassier
	cannorian_30_drill_infantry
}

monarch_names = {

	#Generic Bulwari Names
	"Aban #0" = 10
	"Abazu #0" = 10
	"Absol #0" = 10
	"Adad #0" = 10
	"Akur #0" = 10
	"Aloros #0" = 10
	"Anshar #0" = 10
	"Arad #0" = 10
	"Arashk #0" = 10
	"Arhab #0" = 10
	"Arsham #0" = 10
	"Arvand #0" = 10
	"Ashan #0" = 10
	"Ashur #0" = 10
	"Asor #0" = 10
	"Atbin #0" = 10
	"Athan #0" = 10
	"Awbal #0" = 10
	"Baraz #0" = 10
	"Barid #0" = 10
	"Barseen #0" = 10
	"Bavar #0" = 10
	"Behlil #0" = 10
	"Berk #0" = 10
	"Bullud #0" = 10
	"Bupalos #0" = 10
	"Cahit #0" = 10
	"Dagan #0" = 10
	"Darab #0" = 10
	"Darian #0" = 10
	"Dariush #0" = 10
	"Dartaxes #0" = 10
	"Demartos #0" = 10
	"Demos #0" = 10
	"Durul #0" = 10
	"Duzan #0" = 10
	"Duzar #0" = 10
	"Duzi #0" = 10
	"Ekrem #0" = 10
	"Erbay #0" = 10
	"Erol #0" = 10
	"Erten #0" = 10
	"Fazil #0" = 10
	"Giz #0" = 10
	"Hazan #0" = 10
	"Hermeias #0" = 10
	"Himmet #0" = 10
	"Huran #0" = 10
	"Husaam #0" = 10
	"Hyaclos #0" = 10
	"Idad #0" = 10
	"Idris #0" = 10
	"Ikelos #0" = 10
	"Irran #0" = 10
	"Iskrates #0" = 10
	"Jahangir #0" = 10
	"Jareer #0" = 10
	"Kalib #0" = 10
	"Kaptan #0" = 10
	"Kartal #0" = 10
	"Kaveh #0" = 10
	"Kikud #0" = 10
	"Korydon #0" = 10
	"Laris #0" = 10
	"Larza #0" = 10
	"Limer #0" = 10
	"Lykidas #0" = 10
	"Marod #0" = 10
	"Marodeen #0" = 10
	"Mehran #0" = 10
	"Mesten #0" = 10
	"Nahroon #0" = 10
	"Naram #0" = 10
	"Numair #0" = 10
	"Oktan #0" = 10
	"Oreias #0" = 10
	"Panthes #0" = 10
	"Raafi #0" = 10
	"Rabon #0" = 10
	"Radin #0" = 10
	"Rimush #0" = 10
	"Saamir #0" = 10
	"Sabum #0" = 10
	"Saed #0" = 10
	"Samiun #0" = 10
	"Sargin #0" = 10
	"Sepeh #0" = 10
	"Serim #0" = 10
	"Sharaf #0" = 10
	"Suhlamu #0" = 10
	"Temenos #0" = 10
	"Theron #0" = 10
	"Tozan #0" = 10
	"Tydeos #0" = 10
	"Udish #0" = 10
	"Ufuk #0" = 10
	"Yazkur #0" = 10
	"Zaid #0" = 10
	"Zamman #0" = 10
	"Zimud #0" = 10
	"Zimudar #0" = 10
	
	"Aamina #0" = -10
	"Abella #0" = -10
	"Amata #0" = -10
	"Amina #0" = -10
	"Anatu #0" = -10
	"Aqeela #0" = -10
	"Araha #0" = -10
	"Arahuna #0" = -10
	"Arwia #0" = -10
	"Asiah #0" = -10
	"Baila #0" = -10
	"Benan #0" = -10
	"Canfeda #0" = -10
	"Damrina #0" = -10
	"Derya #0" = -10
	"Dilara #0" = -10
	"Elif #0" = -10
	"Elina #0" = -10
	"Elmas #0" = -10
	"Emmita #0" = -10
	"Ettu #0" = -10
	"Fatma #0" = -10
	"Gula #0" = -10
	"Haala #0" = -10
	"Haamida #0" = -10
	"Hasibe #0" = -10
	"Ia #0" = -10
	"Iltani #0" = -10
	"Ishtara #0" = -10
	"Jannat #0" = -10
	"Kamelya #0" = -10
	"Kishar #0" = -10
	"Kullaa #0" = -10
	"Leja #0" = -10
	"Lubna #0" = -10
	"Mardina #0" = -10
	"Mudam #0" = -10
	"Musheera #0" = -10
	"Nadide #0" = -10
	"Nahrina #0" = -10
	"Nardina #0" = -10
	"Nesrin #0" = -10
	"Ninsunu #0" = -10
	"Nuriye #0" = -10
	"Nusaiba #0" = -10
	"Nuzha #0" = -10
	"Omara #0" = -10
	"Pari #0" = -10
	"Qamara #0" = -10
	"Rahma #0" = -10
	"Saba #0" = -10
	"Sabahat #0" = -10
	"Samra #0" = -10
	"Semiha #0" = -10
	"Seyma #0" = -10
	"Sireen #0" = -10
	"Ura #0" = -10
	"Yamima #0" = -10
	"Yamina #0" = -10
	"Zakiti #0" = -10
	"Zeyni #0" = -10
}

leader_names = {
	#Country Specific
	"of Eloan" "of Crathan�r" "of Gemisle" "of Westend" "of Hopespeak" "of Azebel"

	
	#Province Neighbours
	"of Azka-szel-Azka" "of Aqatbar" "of Deshkumar" "of Akal-Uak" "of Forramaz" "of Vernat"

	
	#Country Neighbours
	"of Baharkand" "of Azka-Evran" "of Birsartan�es" "of Elizna" "of Brasan" "of Irrliam" "of Varamhar" "of Kumarkand" "of Zan�ap" "of Kalib" "of Zanbar" "of Harklum" "of Akal�es" "of Gelkalis"
	 
	#Geographic Neighbours
	"of the Diven" "of Bulwar" "of Tef" "of the Khenak"
	
	#Noble Families
	
	#Generic English names and occupational names
	Abaza Alemdar Amasyali Ayas
	Bali Bayram Bey Boshnak
	Cezzar �elebi Cerrah �erkes Civan �uhadar
	Damad Dervish Defterdar
	Gedik Gazi G�rc�
	Hadim Haseki Hekimoglu Hersekli
	Kemal K���k Koca Karamanli K�se Kara Kemankesh Kaymak Kavalali
	Ishak Izzed
	Lala Lefkeli
	Malko� M�ezzinzade
	Nevshehirli Nishanci
	Oru� Ulu� Ohrili 
	Rumi
	Pasha Piri Piyale Pehlivan
	Salih Sari Silahdar Semiz Serdar Seydi
	Topal Tayyar Turgut Tarhuncu
	Veli
	Yemis�i
	Pashaoglu
	
	#Titles
	"" "'the Bronze'"
	"'the Tall'" "'the Younger'" "'the Clever'" "'the Vigilant'" "'the Stern'" "'the Mighty'" "'Halfblood'" "'the Mudblooded'" "'the Kind'" "'the Able'" "'the Good'" "'the Fat'"
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
	
	#Elven
	Lunetine Agraseina Estaforra
	
	#Regent Court Deities
	Castellos Dame "The Dame" Halanna Ysh Yshtralania Agradls Adean Esmaryal Ryala Edronias Falah Nerat Ara Minara Munas Moonsinger Nathalyne Begga Corin Balgar
	Uelos Drax'os
}

army_names = {
	"Re'uyel Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Re'uyel Fleet" "Copper Squadron" "Stone Squadron" "Bronze Squadron"
}