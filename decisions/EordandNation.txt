country_decisions = {

	eordand_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_eordand }
			NOT = { exists = H21 } #Eordand
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				culture_group = eordellon_ruinborn_elf
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}

		provinces_to_highlight = {
			OR = {
				province_id = 2007
				province_id = 2159
				province_id = 2037
				province_id = 1775
				province_id = 2022
				province_id = 2049
				province_id = 1969
				province_id = 1965
			}
			OR = {
				NOT = { owned_by = ROOT }
				NOT = { is_core = ROOT }
			}
		}
		allow = {
			adm_tech = 7
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			#num_of_cities = 5
			
			#Requires provinces in country or non-sovereign subject
			owns_or_non_sovereign_subject_of = 2007
			owns_or_non_sovereign_subject_of = 2159
			owns_or_non_sovereign_subject_of = 2037
			owns_or_non_sovereign_subject_of = 1775
			owns_or_non_sovereign_subject_of = 2022
			owns_or_non_sovereign_subject_of = 2049
			owns_or_non_sovereign_subject_of = 1969
			owns_or_non_sovereign_subject_of = 1965
			num_of_non_tributary_subjects = 2
			
			
		}
		effect = {
			change_tag = H21
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 3 }
				}
				set_government_rank = 3
			}
			custom_tooltip = tooltip_eordand_culture_provinces
			#Claims
			eordand_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = H21
			}
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			
			add_prestige = 50
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			set_country_flag = formed_eordand_flag
			country_event = { id = eordand.1 days = 1 }
			country_event = { id = eordand.2 days = 2 }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
}