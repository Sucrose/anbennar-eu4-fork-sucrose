country_decisions = {

	grombar_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_grombar_nation }
			NOT = { exists = Z50 }
			OR = {
				NOT = { tag = Z51 }
				NOT = { tag = Z52 }
				NOT = { tag = Z53 }
			}
			
			OR = {
				primary_culture = gray_orc
				AND = {
					primary_culture = green_orc
					num_of_owned_provinces_with = {
						value = 1
						superregion = gerudia_superregion
					}
				}
			}
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 5
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			has_institution = feudalism
			is_free_or_tributary_trigger = yes
			is_at_war = no
			owns_core_province = 740
			num_of_owned_provinces_with = {
				value = 10
				superregion = gerudia_superregion
			}
			OR = { 
				adm_tech = 7	#1492
				NOT = { religion = great_dookan }	#Corinite
			}
		}
		effect = {
			change_tag = Z50
			change_primary_culture = gray_orc
			remove_non_electors_emperors_from_empire_effect = yes
			swap_non_generic_missions = yes
			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_greentide_invaders_modifier"
				duration = 14600
			}
			set_country_flag = orc_nation_formed
			clr_country_flag = knightly_order_adventurer
			country_event = { id = orcform.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			set_country_flag = formed_grombar_nation
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	khozrugan_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_khozrugan_nation }
			NOT = { exists = Z51 }
			OR = {
				NOT = { tag = Z50 }
				NOT = { tag = Z52 }
				NOT = { tag = Z53 }
			}
			
			num_of_owned_provinces_with = {
				value = 1
				region = west_castanor_region
			}
			OR = {
				primary_culture = green_orc
				primary_culture = gray_orc
			}
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 5
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			has_institution = feudalism
			OR = { 
				adm_tech = 8	#1505
				NOT = { religion = great_dookan }
			}
			is_free_or_tributary_trigger = yes
			is_at_war = no
			num_of_owned_provinces_with = {
				value = 10
				region = west_castanor_region
			}
		}
		effect = {
			change_tag = Z51
			change_primary_culture = green_orc
			remove_non_electors_emperors_from_empire_effect = yes
			swap_non_generic_missions = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_greentide_invaders_modifier"
				duration = 14600
			}
			set_country_flag = orc_nation_formed
			clr_country_flag = knightly_order_adventurer
			country_event = { id = orcform.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			set_country_flag = formed_khozrugan_nation
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	unguldavor_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_unguldavor_nation }
			NOT = { exists = Z52 }
			OR = {
				NOT = { tag = Z50 }
				NOT = { tag = Z51 }
				NOT = { tag = Z53 }
			}
			
			num_of_owned_provinces_with = {
				value = 1
				region = inner_castanor_region
			}
			
			OR = {
				primary_culture = green_orc
				primary_culture = gray_orc
			}
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 5
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			has_institution = feudalism
			OR = { 
				adm_tech = 8	#1505
				NOT = { religion = great_dookan }
			}
			is_free_or_tributary_trigger = yes
			is_at_war = no
			num_of_owned_provinces_with = {
				value = 10
				region = inner_castanor_region
			}
		}
		effect = {
			change_tag = Z52
			change_primary_culture = green_orc
			remove_non_electors_emperors_from_empire_effect = yes
			swap_non_generic_missions = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_greentide_invaders_modifier"
				duration = 14600
			}
			set_country_flag = orc_nation_formed
			clr_country_flag = knightly_order_adventurer
			country_event = { id = orcform.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			set_country_flag = formed_unguldavor_nation
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	barumand_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_barumand_nation }
			NOT = { exists = Z53 }
			OR = {
				NOT = { tag = Z50 }
				NOT = { tag = Z51 }
				NOT = { tag = Z52 }
			}
			
			num_of_owned_provinces_with = {
				value = 1
				region = south_castanor_region
			}
			OR = {
				primary_culture = green_orc
				primary_culture = gray_orc
			}
			
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 5
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			has_institution = feudalism
			OR = { 
				adm_tech = 8	#1505
				NOT = { religion = great_dookan }
			}
			is_free_or_tributary_trigger = yes
			is_at_war = no
			num_of_owned_provinces_with = {
				value = 10
				region = south_castanor_region
			}
		}
		effect = {
			change_tag = Z53
			change_primary_culture = green_orc
			remove_non_electors_emperors_from_empire_effect = yes
			swap_non_generic_missions = yes

			add_prestige = 25
			add_country_modifier = {
				name = "legacy_of_greentide_invaders_modifier"
				duration = 14600
			}
			set_country_flag = orc_nation_formed
			clr_country_flag = knightly_order_adventurer
			country_event = { id = orcform.1 days = 1 }
			if = {
				limit = { has_custom_ideas = no }
				country_event = { id = ideagroups.1 } #Swap Ideas
			}
			set_country_flag = formed_barumand_nation
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	orc_change_religion_corinite = {
		major = yes
		potential = {
			culture_group = orcish
			religion = great_dookan
			is_religion_enabled = corinite
			is_free_or_tributary_trigger = yes
			is_force_converted = no
		}
		allow = {
			stability = 2
		}
		effect = {
			add_stability = -3
			change_religion = corinite
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 9125
			}
			capital_scope = {
				change_religion = corinite
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				capital_scope = {
					region = west_castanor_region
				}
				factor = 0.1
			}
			modifier = {
				tag = Z51
				factor = 0
			}
		}
	}
}