namespace = bulwar_flavour

#Resettling Far Bulwar
province_event = {
	id =  bulwar_flavour.1
	title = bulwar_flavour.1.t
	desc = bulwar_flavour.1.d
	picture = COLONIZATION_eventPicture
	
	trigger = {
		region = far_bulwar_region
		NOT = {
			area = sareyand_area
			area = ardu_area
			area = garpix_zokka_area
			area = avamezan_area
		}
		owner = {
			NOT = { 
				culture_group = gnollish
			}
		}
		culture_group = gnollish
		NOT = { is_year = 1550 }
		
		OR = {
			AND = {
				
			}
			NOT = { development = 10 }
		}
	}


	mean_time_to_happen = {
		days = 730
		modifier = {
			factor = 0.25
			primary_culture = surani
		}
		modifier = {
			factor = 0.5
			accepted_culture = surani
		}
		modifier = {
			factor = 3
			OR = {
				has_terrain = desert
				has_terrain = mountain
			}
		}
	}

	option = {
		name = bulwar_flavour.1.a
		
		if = {
			limit = {
				NOT = { development = 5 }
				OR = {
					has_terrain = desert
					has_terrain = mountain
				}
			}
			random_list = {
				1 = { add_base_tax = 1 }
				1 = { add_base_production = 1 }
				1 = { add_base_manpower = 1 }
			}
		}
		else_if = {
			limit = {
				NOT = { development = 10 }
			}
			random_list = {
				1 = { add_base_tax = 1 }
				1 = { add_base_production = 1 }
				1 = { add_base_manpower = 1 }
			}
			random_list = {
				1 = { add_base_tax = 2 }
				1 = { add_base_production = 2 }
				1 = { add_base_manpower = 2 }
			}
		}
		change_culture = surani
		change_religion = bulwari_sun_cult
		
		add_province_modifier = { name = add_unrest_5_modifier duration = 730 }
		add_devastation = 10
	}
	
	option = {
		name = bulwar_flavour.1.b
		
		owner = {
			add_prestige = -5
		}
	}
}