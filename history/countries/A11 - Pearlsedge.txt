government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = pearlsedger
religion = regent_court
technology_group = tech_cannorian
capital = 43 #Pearlsedge
national_focus = DIP

elector = yes

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1427.3.11 = {
	monarch = {
		name = "Caylen V"
		dynasty = "s�l na Tr�sfer"
		birth_date = 1423.11.4
		adm = 5
		dip = 4
		mil = 2
	}
}