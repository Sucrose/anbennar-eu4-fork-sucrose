government = theocracy
add_government_reform = leading_clergy_reform
government_rank = 1
primary_culture = kheteratan
religion = khetist
technology_group = tech_salahadesi
national_focus = DIP
capital = 473

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Nahab the Defiant"
		birth_date = 1413.7.3
		adm = 3
		dip = 3
		mil = 3
	}
}