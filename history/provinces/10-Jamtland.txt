#J�mtland+H�rjedalen

owner = A04
controller = A04
add_core = A04
culture = west_damerian
religion = regent_court
hre = yes
base_tax = 7
base_production = 7
trade_goods = cloth
base_manpower = 4
capital = ""
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_small
	duration = -1
}