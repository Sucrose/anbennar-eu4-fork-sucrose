#123 - Calabria

owner = A01
controller = A01
add_core = A01
culture = low_lorentish
religion = regent_court
hre = no
base_tax = 3
base_production = 4
trade_goods = fish
base_manpower = 2
capital = "" 
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
