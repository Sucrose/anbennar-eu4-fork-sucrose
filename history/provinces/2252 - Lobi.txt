# No previous file for Lobi
culture = thamvoii
religion = tswohvwohii
capital = ""

hre = no

base_tax = 2
base_production = 3
base_manpower = 3

trade_goods = unknown

native_size = 20
native_ferocity = 2
native_hostileness = 1

discovered_by = tech_effelai