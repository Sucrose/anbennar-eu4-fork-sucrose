#236 - London
#Middlesex+Hertfordshire+Surrey+parts of Kent and Essex

owner = A36
controller = A36
add_core = A36
culture = arbarani
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 4

trade_goods = cloth

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
