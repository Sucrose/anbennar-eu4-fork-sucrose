#Brest-Litovsk

owner = A45
controller = A45
add_core = A45
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 6
base_production = 7
base_manpower = 7

trade_goods = iron

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish


add_permanent_province_modifier = {
	name = elven_minority_coexisting_small
	duration = -1
}


add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_small
	duration = -1
}