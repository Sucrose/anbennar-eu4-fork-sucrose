# No previous file for Banten
owner = F47
controller = F47
add_core = F47
culture = gold_dwarf
religion = ancestor_worship
center_of_trade = 1
fort_15th = yes

hre = no

base_tax = 8
base_production = 7
base_manpower = 9

trade_goods = gold

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari