#300 - Tula

owner = A34
controller = A34
add_core = A34
culture = wexonard
religion = regent_court

hre = yes

base_tax = 4
base_production = 3
base_manpower = 4

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish


add_permanent_province_modifier = {
	name = halfling_minority_coexisting_large
	duration = -1
}