#377 - Aleppo

owner = A49
controller = A49
add_core = A49
culture = hill_gnoll
religion = xhazobkult

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = copper

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
