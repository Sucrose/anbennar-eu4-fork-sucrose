#413 - Sorncell

owner = A56
controller = A56
add_core = A56
culture = arannese
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 3

trade_goods = fish
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_small
	duration = -1
}