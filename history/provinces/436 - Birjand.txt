#436 - Birjand

owner = A59
controller = A59
add_core = A59
culture = corvurian
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 3

trade_goods = iron

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
