#Sk�ne

owner = A85
controller = A85
add_core = A85
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 5
base_production = 5
base_manpower = 4

trade_goods = wool #GOATS CHEESE

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
