# No previous file for Hejian
owner = Z22
controller = Z22
add_core = Z22
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
discovered_by = tech_orcish