# No previous file for Kobdo
culture = gray_orc
religion = great_dookan
capital = ""
trade_goods = unknown

hre = no

base_tax = 3
base_production = 2
base_manpower = 2

native_size = 24
native_ferocity = 8
native_hostileness = 6