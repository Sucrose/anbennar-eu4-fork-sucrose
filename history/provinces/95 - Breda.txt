# 95 Breda | 

owner = A14
controller = A14
add_core = A14
culture = high_lorentish
religion = regent_court
hre = no
base_tax = 5
base_production = 5
trade_goods = cloth
base_manpower = 3
capital = ""
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = half_elven_minority_coexisting_small
	duration = -1
}