# No previous file for Fegras
owner = Z08
controller = Z08
add_core = Z08
culture = west_dalr
religion = skaldhyrric_faith

hre = no

base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind
