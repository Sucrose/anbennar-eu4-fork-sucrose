# No previous file for Revrhavn
owner = Z10
controller = Z10
add_core = Z10
culture = west_dalr
religion = skaldhyrric_faith

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = naval_supplies
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind

add_permanent_province_modifier = {
	name = dwarven_minority_coexisting_small
	duration = -1
}