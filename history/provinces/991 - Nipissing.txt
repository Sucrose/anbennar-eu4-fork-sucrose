# No previous file for Nipissing
culture = gray_orc
religion = great_dookan
capital = ""
trade_goods = unknown

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

native_size = 16
native_ferocity = 8
native_hostileness = 6