# No previous file for Sidmargbal
owner = H06
controller = H06
add_core = H06
culture = caamas
religion = eordellon
capital = "Sidmargbal"
fort_15th = yes

hre = no

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = fur

native_size = 14
native_ferocity = 6
native_hostileness = 6